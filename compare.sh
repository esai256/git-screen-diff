BASEDIR=$(dirname "$0")
BRANCH_NAME=$1
TESTING_COMMAND=$2
URL=$3
PREPARATION_SCRIPT_FILE=$4
DELAY=$5

google-chrome --headless --hide-scrollbars --remote-debugging-port=9222 --disable-gpu &
CHROME_PID=$!

CURRENT_BRANCH=$(git branch | grep \* | cut -d ' ' -f2)

git stash;

git fetch;
git checkout $BRANCH_NAME;

sleep ${5:-0.5}

$($TESTING_COMMAND) &

node "$BASEDIR/index.js" --url=$URL --preparationScriptFile=$PREPARATION_SCRIPT_FILE --outputFileName="output-$BRANCH_NAME";

git checkout $CURRENT_BRANCH;

git stash apply;
git stash drop;

sleep ${5:-0.5}

$($TESTING_COMMAND) &

node "$BASEDIR/index.js" --url=$URL --preparationScriptFile=$PREPARATION_SCRIPT_FILE --outputFileName="output-$CURRENT_BRANCH";

kill -HUP $CHROME_PID;

node "$BASEDIR/diff.js" --img1="output-$CURRENT_BRANCH.png" --img2="output-$BRANCH_NAME.png"
