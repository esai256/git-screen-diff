const fs = require('fs');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const argv = require('minimist')(process.argv.slice(2));

const img1 = fs.createReadStream(argv.img1).pipe(new PNG()).on('parsed', doneReading);
const img2 = fs.createReadStream(argv.img2).pipe(new PNG()).on('parsed', doneReading);
let filesRead = 0;

function doneReading() {
  if (++filesRead < 2) return;
  const diff = new PNG({ width: img1.width, height: img1.height });

  pixelmatch(img1.data, img2.data, diff.data, img1.width, img1.height, { threshold: 0.1 });

  diff.pack().pipe(fs.createWriteStream('diff.png'));
}
