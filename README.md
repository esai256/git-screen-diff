# git-screen-diff

Automatically taking 2 Screenshots of the same repository out of different branches.

with a script from this post: [link](https://medium.com/@dschnr/using-headless-chrome-as-an-automated-screenshot-tool-4b07dffba79a)

Usage:

`./compare.sh [branch-name] [command to execute] [url which should be captured] [path to eventually needed manipulating js] [wait-time for command to finish in seconds]`

Example:

`./compare.sh master 'http-server .' 'http://localhost:8080' './test.js' 5`
